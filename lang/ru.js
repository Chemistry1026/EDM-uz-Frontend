export default () => {
    return new Promise(function (resolve) {
        resolve({
            navigation: {
                home: 'Главная',
                tracks: 'Треки',
                artists: 'Артисты',
                genres: 'Жанры',
                about: 'О нас',
                events: 'События',
                news: 'Новости',
            },

            chart: {
                day: 'За день',
                week: 'За неделю',
                chart: 'Топ чарт',
            },

            titles: {
                week_artists: 'Артисты недели',
                our_tracks: 'Наши треки',
                last_tracks: 'Последние треки',
            },

            other: {
                search: 'Поиск...',
            }
        })
    });
}
