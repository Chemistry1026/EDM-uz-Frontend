export default () => {
    return new Promise(function (resolve) {
        resolve({
            navigation: {
                home: 'Home',
                tracks: 'Tracks',
                artists: 'Artists',
                genres: 'Genres',
                about: 'About us',
                events: 'Events',
                news: 'News',
            },

            chart: {
                day: 'Per day',
                week: 'Per week',
                chart: 'Top chart',
            },

            titles: {
                week_artists: 'Artists of the week',
                our_tracks: 'Our tracks',
                last_tracks: 'Last tracks',
            },

            other: {
                search: 'Search...',
            }
        })
    });
}
