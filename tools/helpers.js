import Vue from 'vue'
// import { msg as msgForThisFile } from '~/tools/helpers'

const getFuncForShowMsg = (mainType) => (title, message, type, timeout) => {
  // Vue.prototype.$notify не будет доступен если просто записать в экспортированную переменную которую
  const notify = Vue.prototype.$notify
  notify(title, message, mainType, timeout)
}

// массив функций обозначающих тип сообщения
export const msg = {
  success: getFuncForShowMsg('success'),
  info: getFuncForShowMsg('info'),
  warn: getFuncForShowMsg('warn'),
  error: getFuncForShowMsg('error')
}
