class Errors {
    constructor(){
        this.errors = {};
    }

    get(field){
        if(this.errors[field]){
            return this.errors[field][0];
        }
    }

    any(){
        return Object.keys(this.errors).length > 0;
    }

    has(field){
        if(this.errors)
            return this.errors.hasOwnProperty(field);
    }

    clear(field){
        if(field) return delete this.errors[field];

        this.errors = {};
    }

    record(errors){
        this.errors = errors.errors
    }
}

export default Errors;
