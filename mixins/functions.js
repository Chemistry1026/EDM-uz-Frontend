import Vue from 'vue';

const Functions = {
    install(Vue){
        Vue.mixin({
            methods: {
                setToken(token){
                    let bearer = `Bearer ${token}`;
                    localStorage.setItem('Authorization', bearer);
                    this.$cookies.set('Authorization', encodeURI(bearer));
                },

                async login(data, message = true) {
                    this.disabled = true;

                    try {
                        let { access_token } = await this.$axios.$post('/login', data);
                        this.setToken(access_token);
                        await this.$store.dispatch('user/user');

                        if(message) {
                            this.$notify.success('Вы успешно авторизовались');
                        }
                        this.$router.push('/');
                    } catch (e) {
                        this.$notify.error('Ошибка неверный логин или пароль!');
                    }

                    this.disabled = false;
                },

                getToken(){
                    return decodeURI(this.$cookies.get('Authorization'));
                },

                async logout(){
                    try {
                        let { message } = await this.$store.dispatch('user/logout');
                        this.$notify.success(message);
                    } catch (e) {

                    }

                    this.$cookies.remove('Authorization');
                    localStorage.removeItem('Authorization');
                    this.$router.push('/');
                },
            },
        })
    }
};

Vue.use(Functions);
