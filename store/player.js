export const state = () => ({
    playlist: [],
    tempPlaylist: [],
    current: {},
    playing: false,
    positions: [],
    random: [],
    radio: null,
    url: '',
});

export const getters = {
    playlist: state => state.playlist,
    tempPlaylist: state => state.tempPlaylist,
    current: state => state.current,
    playing: state => state.playing,
    radio: state => state.radio,
    url: state => state.url,
};

export const actions = {

};

export const mutations = {
    toggleRadio(state){
        state.radio = !(state.radio);
        if(!state.radio) {
            state.current = {};
            state.url = '';
        }
    },

    setCurrentTrack(state, payload){
        state.current = payload;
        if(!state.radio || !state.url) {
            state.url = payload.mp3;
        }
    },

    changePlayerState(state, payload){
        state.playing = payload;
    },

    changePosition(state){
        let temp = state.tempPlaylist;
        let positions = [];
        for(let i in temp){
            positions.push(i);
        }

        state.positions = positions;
    },

    randomPositions(state){
        state.positions.sort((a, b) => {
            return Math.random() - 0.5;
        });
    },

    pushToTempPlaylist(state, payload){
        let tempPlaylist = state.tempPlaylist;
        let findedTrack = tempPlaylist.filter(item => {
            return item.id === payload.id
        });

        if(!findedTrack.length) {
            tempPlaylist.push(payload);
        }
        // for(let i in tempPlaylist){
        //     if(tempPlaylist[i].id !== payload.id){
        //         tempPlaylist.push(payload);
        //     }
        // }
        //
        // if(!tempPlaylist.length){
        //
        // }

        state.tempPlaylist = tempPlaylist;
        // this.commit('changePosition');
    },

    pushToPlaylist(state, payload){
        let playlist = state.playlist;
        for(let i in playlist){
            if(playlist[i].id !== payload.id){
                playlist.push(payload);
            }
        }

        if(!playlist){
            playlist.push(payload);
        }

        state.playlist = playlist;
    },

    removeFromTempPlaylist(state, payload){
        let tempPlaylist = state.tempPlaylist;
        for(let i in tempPlaylist){
            if(i === payload.id){
                tempPlaylist.splice(i, 1);
            }
        }

        state.tempPlaylist = tempPlaylist;
        // this.commit('changePosition');
    }
};
