export const state = () => ({
    loggedIn: false,
    user: null,
});

export const mutations = {
    SET_USER(state, payload) {
        state.user = payload;
        state.loggedIn = true;
    },

    updateUser: (state, payload) => state.user = payload,
    REMOVE_USER: state => {
        state.loggedIn = false;
        state.user = null;
    },
};

export const actions = {
    // async login({ dispatch }, credentials) {
    //
    // },

    async user({ commit } ) {
        try {
            let { user } = await this.$axios.$get('/user');
            commit('SET_USER', user);
        } catch (e) {

        }
    },

    async logout({ commit }){
        await this.$axios.$post('/logout').then(() => {
            commit('REMOVE_USER');
        })
    },
};

export const getters = {
    loggedIn: state => state.loggedIn,
    user: state => state.user,
};
