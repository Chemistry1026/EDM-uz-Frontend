export const state = () => {

};

export const getters = {

};

export const actions = {
    async nuxtServerInit ({ dispatch, state}, { app, req } ) {
        let token = app.$cookies.get('Authorization');
        if(token){
            await dispatch('user/user');
        }
    },
};

export const mutations = {

};
