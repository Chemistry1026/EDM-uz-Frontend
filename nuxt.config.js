require('dotenv').config();
const pkg = require('./package');

module.exports = {
  mode: 'universal',
  env: {
    baseUrl: process.env.API_URL || 'http://localhost:3000',
    fullApiUrl: process.env.FULL_API_URL || 'http://localhost:7000',
    apiURL: process.env.API_URL || 'http://localhost:7000/front',
  },

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: {
    color: '#ffc107',
    height: '4px'
  },

  /*
  ** Global CSS
  */
  css: [
    { src: '~/assets/scss/style.scss', lang: 'scss' },
    { src: '~/assets/scss/responsive.scss', lang: 'scss' },
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
      { src: '~/plugins/audio-player' },
      { src: '~/plugins/vue-awesome-swiper' },
      // { src: '~/plugins/audio-uploader.client' },
      { src: '~/plugins/media.client' },
      { src: '~/plugins/axios' },
      { src: '~/plugins/vue-notifications', ssr: false },

      { src: '~/mixins/functions' },
      { src: '~/mixins/directives' },
      { src: '~/mixins/filters' },
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/dotenv',
    'cookie-universal-nuxt',
    ['nuxt-i18n', {
      defaultLocale: 'ru',
      locales: [
        {
          code: 'ru',
          iso: 'ru_RU',
          file: 'ru.js'
        },
        {
          code: 'en',
          iso: 'en-US',
          file: 'en.js'
        },
        {
          code: 'uz',
          iso: 'uz_UZ',
          file: 'uz.js'
        }
      ],
      lazy: true,
      langDir: 'lang/'
    }]
  ],

  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    extractCSS: true,

    extend (config, { isDev, isClient }) {
      config.node = {
        fs: 'empty'
      };
    },
    /*
    ** You can extend webpack config here
    */
    // extend(config, ctx) {
    //   // Run ESLint on save
    //   if (ctx.isDev && ctx.isClient) {
    //     config.module.rules.push({
    //       enforce: 'pre',
    //       test: /\.(js|vue)$/,
    //       loader: 'eslint-loader',
    //       exclude: /(node_modules)/
    //     })
    //   }
    // }
  }
}
