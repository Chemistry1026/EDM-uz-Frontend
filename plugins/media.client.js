import Vue from 'vue';
import MediaQuery from '~/components/MediaQuery';

Vue.component('MediaQuery', MediaQuery);
