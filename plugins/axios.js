export default function ({ $axios, redirect, $route, app }) {
    $axios.onRequest((config) => {
        let token = app.$cookies.get('Authorization');
        if(token){
            config.headers.common['Authorization'] = decodeURI(token);
        }
        config.headers.common['x-nuxt-access'] = true;
    });

    $axios.onResponseError(({ response }) => {
        const code = parseInt(response && response.status);
        if (code === 401) {
            app.$cookies.remove('Authorization');
            if(window.location.pathname !== '/login')
                window.location = `${window.location.origin}/login`;
        }
    });
}
