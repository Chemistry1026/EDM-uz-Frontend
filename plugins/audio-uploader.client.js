import Vue from 'vue';
import AudioUploader from '~/components/Uploader/Audio';

Vue.component('AudioUploader', AudioUploader);
