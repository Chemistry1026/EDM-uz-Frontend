import Vue from 'vue';
import AudioPlayer from '../components/Audio/AudioPlayer';

Vue.component('audio-player', AudioPlayer);
